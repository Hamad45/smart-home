#include<reg51.h>
#include<LCD.h>
#include<Uart_Data.h>
void pinsInit(); // PINS INITIALIZE FUNCTION
void data_check();// RECEIVED DATA CHECK FUNCTION
void pins_check();//SENSOR PINS CHECK FUNCTION
void main()
{	
  //P1=0x1D;
	//P2=0x00;
	
	pinsInit(); // INITIALIZING I/O PINS
	LcdInit();// INITIALIZING LCD
	Uart_Init(); //INITIALIZING SERIAL COMMUNICATION
	Lcd(0x82,0); // SETTING OPTIMUM CURSOR POSITION FOR DISPLAY
	LcdPrint("SYSTEM READY");//DISPLAYING READY STATE AND WAITING FOR INPUT FROM SMARTPHONE
	
	while(1) //REPEAT THESE 2 FUNCTIONS ALWAYS
	{ 
		if(RI) data_check(); //IF DATA IS RECEIVED THEN CHECK FOR VALID CASE
		pins_check();	//CHECK SENSOR PINS 
	}
}


