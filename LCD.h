#include<reg51.h>
sbit rs = P2^5; //PINS INITIALIZATION
sbit rw = P2^6;
sbit en = P2^7;
void delay(int d) //DELAY SUBROUTINE
{
	int i,j;
	for(i=0;i<=d;i++)
		for(j=0;j<=1275;j++);
}

void Lcd(char a,int b)//LCD FUNCTION(CHAR,(DATA/COMMAND))
{
	P0=a;
	rw=0;
	rs=b;
	en=1;
	delay(5);
	en=0;
}

void LcdInit() //LCD INITIALIZATION FUNCTION
{
	P0=0x00;
	rs=0;
	rw=0;
	en=0;
	Lcd(0x38,0);
	delay(5);
	Lcd(0x0C,0);
	delay(5);
	Lcd(0x01,0);
	delay(5);
	Lcd(0x06,0);
	delay(5);
	Lcd(0x80,0);
	delay(5);
}

void LcdPrint(char *msg) //PRINT LCD DATA
{
	int c;
	for(c=0;msg[c]!='\0';c++)
		Lcd(msg[c],1);
}
