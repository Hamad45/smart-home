#include<reg51.h>

/* DEVICE PINS*/
sbit fan = P2^0;
sbit tv = P2^1;
sbit router = P2^2;
sbit bulb = P2^3;
sbit buzzer = P2^4;
sbit b_door=P2^5;
sbit pump = P2^6;
sbit h_level = P1^3;
sbit l_level = P1^4;

/*SENSORS*/
sbit door = P1^0;
sbit pir = P1^1;
sbit lpg_gd = P1^2;
void pump_check();


void Uart_Init() //INITIALIZE SERIAL COMMUNICATION
{
	TMOD=0x20;
	TH1=0xFD;
	SCON=0x50;
	TR1=1;
}    
char UART_receive() // DATA RECEPTION FUNCTION
{ 
	char rx;
	while(RI==0);
	rx=SBUF;
	RI=0;
	return rx;
}
void pump_check() //CHECK FOR HIGH AND LOW WATER LEVELS
{
	if( (l_level==1 || l_level==0) && h_level==0 ) pump=1;
	if( l_level==1 && h_level==1 ) pump=0; else pump=0;
}
void data_check() // CHECK RECEIVED DATA WITH VALID CASE
{
		char data_in=UART_receive();//STORING RECEIVED CHARACTER INTO A VARIBLE (data_in)
		switch(data_in)
				{	
					case '0': break;
					case '1': Lcd(0x80,0); LcdPrint("DOOR SECURITY ON");
										while(data_in=='1'){
										if(door==1)           // IF DOOR IS OPENED ALARM AND PRINT "INTRUDER"
                      {buzzer=1;bulb=1;Lcd(0x01,0);Lcd(0x83,0);LcdPrint("INTRUDER!!");}
										else{buzzer=0;bulb=0;}}break;												 
											/* DEVICES ON/OFF */
					case '2': fan=1;Lcd(0x01,0); Lcd(0x85,0);LcdPrint("FAN ON"); break;
					case '3': fan=0; Lcd(0x01,0);Lcd(0x85,0);LcdPrint("FAN OFF"); break;
					case '4': tv=1; Lcd(0x01,0);Lcd(0x85,0);LcdPrint("TV ON"); break;
					case '5': tv=0;Lcd(0x01,0); Lcd(0x85,0);LcdPrint("TV OFF"); break;
					case '6': router=1; Lcd(0x01,0);Lcd(0x83,0);LcdPrint("ROUTER ON"); break;
					case '7': router=0; Lcd(0x01,0);Lcd(0x83,0);LcdPrint("ROUTER OFF"); break;
					case '8': bulb=1;Lcd(0x01,0);Lcd(0x84,0); LcdPrint("BULB ON"); break;
					case '9':	bulb=0;Lcd(0x01,0);Lcd(0x84,0); LcdPrint("BULB OFF"); break;
					/* WATER LEVEL DETECION*/
					case 'A': Lcd(0x80,0); LcdPrint("AUTO WATER FILL");
											while(h_level==0)
												{pump_check();}
										Lcd(0x01,0);Lcd(0x80,0); LcdPrint("WATER FILLED");break;
										/*while(data_in=='A')
										{
											if( (l_level==1 || l_level==0) && h_level==0 ) pump=1;
											if( l_level==1 && h_level==1 ) pump=0;	
										} break;*/
				  case 'O': break;
					default:  break;
				}
		}
	void pins_check() // CHECK SENSOR PINS
{ 
	
	if(lpg_gd==0){buzzer=1;Lcd(0x01,0);Lcd(0x81,0);LcdPrint("GAS LEAKING");} //LPG PROVIDES LOW SIGNAL
	if(pir==1) b_door=1; else b_door=0;//PIR PROVIDES HIGH SIGNAL
	
}
void pinsInit() //PINS INITIALIZE DURING STARTUP 
{
	P0=0x00; //DECLARING OUTPUT PORT
	
	//rs=rw=en=0;//LOWERING OUTPUT PINS
	pir=h_level=l_level=lpg_gd=door=1;//DECLARING INPUT PINS
	pir=h_level=l_level=0;//LOWERING INPUT PINS
	lpg_gd=door=1;//SETTING INPUT PINS HIGH
	P2=0x00; //DECLARING OUTPUT PORT
  fan=bulb=tv=router=buzzer=b_door=pump=0;//LOWERING DEVICE PINS INITIALLY
  rs=rw=en=0;//LOWERING OUTPUT PINS
}